package database;

import android.provider.BaseColumns;

public class DefinirTabla {
    public DefinirTabla(){

    }

    public static abstract class Contacto implements BaseColumns{
        public static final String TABLE_NAME="contactos";
        public static final String NOMBRE="nombre",
        TELEFONO1="telefono1",TELEFONO2="telefono2",DOMICILIO="domicilio",
        NOTAS="notas",FAVORITO="favorito";
    }
}
